from pathlib import Path

ROOT = Path(__file__).parent
REGISTRY = {}


def init():
    if not REGISTRY:
        for path in ROOT.iterdir():
            if path.suffix != ".html":
                continue
            name = str(path.relative_to(ROOT))
            REGISTRY[name] = path.read_text()


def get(name):
    return REGISTRY[name]
