import minicli

from . import app, templates
from roll.extensions import simple_server


@minicli.cli
def serve(reload=False):
    """Run a web server (for development only)."""

    templates.init()
    files = [str(templates.ROOT / name) for name in templates.REGISTRY]
    if reload:
        import hupper

        reloader = hupper.start_reloader("canaliens.bin.serve")
        # FIXME: Never called
        reloader.watch_files(files)
    simple_server(app, port=9393)


def main():
    minicli.run()
