import json
from pathlib import Path

from roll import Roll, HttpError
from roll.extensions import cors, traceback

from . import templates


app = Roll()
cors(app)
traceback(app)

ROOT = Path(__file__).parent
DATA_ROOT = Path("/tmp")


@app.listen("startup")
async def on_startup():
    templates.init()


@app.route("/")
async def home(request, response):
    response.redirect = "/suivre", 302


@app.route("/envoyer")
async def envoyer(request, response):
    response.headers["Content-Type"] = "text/html; charset=utf-8"
    response.body = templates.get("envoyer.html")


@app.route("/suivre")
async def suivre(request, response):
    response.headers["Content-Type"] = "text/html; charset=utf-8"
    response.body = templates.get("suivre.html")


@app.route("/seraphine.geojson", methods=["PUT"])
async def persist(request, response):
    data = request.json
    lat = data.get("lat")
    lng = data.get("lng")
    geojson = {
        "type": "Feature",
        "geometry": {"type": "Point", "coordinates": [lng, lat]},
        "properties": {
            "name": "Séraphine",
            "accuracy": data.get("accuracy"),
            "heading": data.get("heading"),
            "speed": data.get("speed"),
            "timestamp": data.get("timestamp"),
        },
    }
    storage = DATA_ROOT / "seraphine.geojson"
    storage.write_text(json.dumps(geojson))
    response.status = 204


@app.route("/seraphine.geojson")
async def consult(request, response):
    storage = DATA_ROOT / "seraphine.geojson"
    if not storage.exists():
        raise HttpError(404)
    response.body = storage.read_text()
