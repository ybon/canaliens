.ONESHELL:
include .env
HOST=ferment
RUN:=ssh $(HOST)
WITH_SUDO:=$(RUN) sudo
WITH_USER:=$(RUN) sudo -u canaliens
WITH_ENV:=$(RUN) "set -o allexport; source /srv/canaliens/env; set +o allexport; sudo --user canaliens --preserve-env"
CLI:=$(RUN) "set -a; . /srv/canaliens/env; set +a; /srv/canaliens/venv/bin/canaliens"
VENV=/srv/canaliens/venv/
PIP:=$(WITH_USER) $(VENV)/bin/pip
# Export env to child processes (eg. python scripts).
export

help: ## This help.
	@grep -E '^[\.a-zA-Z_-]+:.*?## .*$$' Makefile | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.DEFAULT_GOAL := help

remote.pyenv:
	# TODO make sure this command is idempotent
	$(WITH_USER) /usr/bin/python3.9 -m venv $(VENV)
	$(PIP) install pip wheel --upgrade


remote.system:  ## Install system dependencies
	$(WITH_SUDO) apt update
	$(WITH_SUDO) apt install -y software-properties-common nginx git gcc unzip pkg-config rsync python3.9 python3.9-venv python3.9-dev
	$(WITH_SUDO) useradd -m -d /srv/canaliens -s /bin/bash -c "Canaliens System User" -b /srv/ canaliens --system || exit 0

remote.updatepy:
	$(PIP) install -U pip==20.1
	$(PIP) install -U git+https://framagit.org/ybon/canaliens@main#egg=canaliens\\[deploy]


remote.env: build/env  ## Update env on the server
	$(WITH_USER) cat /srv/canaliens/env
	rsync --checksum --rsync-path="sudo --user canaliens rsync" build/env $(HOST):/srv/canaliens/env
	$(WITH_USER) cat /srv/canaliens/env

remote.update: remote.env remote.updatepy
	rsync --checksum --rsync-path="sudo --user canaliens rsync" conf/gunicorn.conf $(HOST):/srv/canaliens/gunicorn.conf.py
# 	rsync --checksum --rsync-path="sudo rsync" conf/hourly.sh $(HOST):/etc/cron.hourly/canaliens
# 	rsync --checksum --rsync-path="sudo rsync" conf/daily.sh $(HOST):/etc/cron.daily/canaliens
# 	rsync --checksum --rsync-path="sudo rsync" conf/monthly.sh $(HOST):/etc/cron.monthly/canaliens

remote.restart:  ## Restart Gunicorn and Nginx.
	$(WITH_SUDO) systemctl restart canaliens nginx

remote.deploy: remote.update remote.restart ## Update python and javascript code on the server and restart.

remote.http:  ## Update HTTP configuration on the server.
	rsync --checksum --rsync-path="sudo rsync" conf/nginx.conf $(HOST):/etc/nginx/sites-enabled/canaliens
	$(WITH_SUDO) systemctl restart nginx

remote.service:
	rsync --checksum --rsync-path="sudo rsync" conf/canaliens.service $(HOST):/etc/systemd/system/canaliens.service
	$(WITH_SUDO) systemctl enable canaliens.service

remote.bootstrap: remote.system  remote.pyenv remote.update remote.service remote.http remote.restart  ## Bootstrap new server

build/env: SHELL := python3
build/env:
	from pathlib import Path
	import os
	env = dict(l.split("=", maxsplit=1) for l in Path(".env").read_text().split("\n") if l.startswith("CANALIENS_"))
	with Path("build/env").open("w") as f:
	    f.write("\n".join(f"{k}={v}" for k, v in env.items()) + "\n")
.PHONY: build/env

local.develop:
	pip install -e .[dev,test]
